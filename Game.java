public class Game {

    private static final String[] RANKS = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"};

    public String winner(String[] deckSteve, String[] deckJosh) {
        int scoreSteve = 0;
        int scoreJosh = 0;

        for (int i = 0; i < deckSteve.length; i++) {
            int rankSteve = getCardRank(deckSteve[i]);
            int rankJosh = getCardRank(deckJosh[i]);

            if (rankSteve > rankJosh) {
                scoreSteve++;
            } else if (rankSteve < rankJosh) {
                scoreJosh++;
            }
        }

        if (scoreSteve > scoreJosh) {
            return String.format("%s wins %d to %d", "Steve", scoreSteve, scoreJosh);
        } else if (scoreSteve < scoreJosh) {
            return String.format("%s wins %d to %d", "Josh", scoreJosh, scoreSteve);
        } else {
            return "Tie";
        }
    }

    private int getCardRank(String card) {
        for (int i = 0; i < RANKS.length; i++) {
            if (card.equals(RANKS[i])) {
                return i + 2;
            }
        }
        return -1;
    }
}

